import 'package:flutter/material.dart';

void main() => runApp(HelloFlutterApp());

class HelloFlutterApp extends StatefulWidget {
  @override
  _HelloFlutterAppState createState() => _HelloFlutterAppState();
}

String englishGreeting = "Hello Flutter !";
String koreanGreeting = "안녕하세요 플러터입니다 !";
String japanGreeting = "こんにちはフラッター ";
String runeGreeting = "ᚺᛖᛚᛚᛟ ᚠᛚᚢᛏᛏᛖᚱ !";

class _HelloFlutterAppState extends State<HelloFlutterApp> {
  String displayText = runeGreeting;
  int display = 1;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Hello Flutter'),
          leading: const Icon(Icons.home),
          actions: <Widget>[
            IconButton(
                onPressed: () {
                  setState(() {
                    display = (display + 1) % 2;
                    if (display == 1) {
                      displayText = englishGreeting;
                    } else {
                      displayText = runeGreeting;
                    }
                  });
                },
                icon: const Icon(Icons.g_translate)),
            IconButton(
                onPressed: () {
                  setState(() {
                    display = (display + 1) % 2;
                    if (display == 1) {
                      displayText = englishGreeting;
                    } else {
                      displayText = koreanGreeting;
                    }
                  });
                },
                icon: const Icon(Icons.g_translate)),
            IconButton(onPressed: () {
              setState(() {
                display = (display+1)%2;
                if ( display == 1 ) {
                  displayText = englishGreeting;
                } else {
                  displayText = japanGreeting;
                }
              });
            }, icon: const Icon(Icons.g_translate)),
          ],
        ),
        body: Center(
          child: Text(displayText, style: TextStyle(fontSize: 24)),
        ),
      ),
    );
  }
}

//  class HelloFlutterApp extends StatelessWidget {
//    @override
//    Widget build(BuildContext context) {
//      return MaterialApp(
//        debugShowCheckedModeBanner: false,
//        home: Scaffold(
//          appBar: AppBar(
//            title: const Text('Hello Flutter'),
//            leading: const Icon(Icons.home),
//            actions: <Widget>[
//              IconButton(onPressed: () {}, icon: const Icon(Icons.refresh))
//            ],
//         ),
//         body: const Center(
//           child: Text("Hello Flutter !", style: TextStyle(fontSize: 24)),
//         ),
//       ),
//     );
//   }
// }
